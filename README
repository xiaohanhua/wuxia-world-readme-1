**Wuxia World README - Table of Contents**  
  
  
  
[TOC]
  
  
# About
----------------------------
__*Wuxia World*__ is an open-source, non-commercial project created by 《LonelyScrub8》/《孤独的Scrub 8》.

Assets used in the game come from Creative Commons and other sources on the internet.
All asset copyrights belong to their respective owners. Check the Attribution section 
to see attribution information. To request modification to attribution info and/or removal 
of assets, please contact 《LonelyScrub8》/《孤独的Scrub 8》 at **LonelyScrub8@gmail.com**

Game copyright belongs to《LonelyScrub8》/《孤独的Scrub 8》. This project can NOT be 
re-distributed for commercial purposes under any circumstances.

# Downloading the game
----------------------------
OS: Windows 10 (recommended)

Download [WuxiaWorldBeta1.3.zip](https://rebrand.ly/3j21nps) 



# Release Notes

### Beta 1.3
----------------------------
* Safe exit for non-critical minigames

* Allow carry over of full stack of 1 item

* Current location indicator 

* Location/Travel time description on tooltips 

* In-game time system 

* Seasonal occurrences 

* Additional plot/quests/characters

* Additional moves

* Bug fixes


### Beta 1.2
----------------------------
* Bug fixes

* Finished Babao Tower 

* Enhanced gravity (lower gravity to a certain extent depending on speed/dexterity)

* Horizontal moving speed increases (to a certain extent) based on speed attribute

* Added hidden Potter's Field plot/minigame

* Added new equipment/moves/skills

* Improved Huashanlunjian rewards


### Beta 1.1
----------------------------
* Bug fixes


# Transferring Save Slots between versions
----------------------------
To transfer save slots from 1 version to the other:

1) Find the Resources\SaveSlots folder in the old version.

![Transfer1](https://bitbucket.org/WuxiaScrub/wuxia-world-readme/raw/108b64874f3297364ce251183da9252435119b9c/Screenshots/SaveSlotTransfer1.PNG)

![Transfer2](https://bitbucket.org/WuxiaScrub/wuxia-world-readme/raw/108b64874f3297364ce251183da9252435119b9c/Screenshots/SaveSlotTransfer2.PNG) 


2) Copy and save all files in the SaveSlots folder.

3) Locate the Resources\SaveSlots folder in the new downloaded version (Folder should be empty).

4) Paste the save slot files from the previous version into the empty SaveSlots folder in the new version.

5) Launch the new version by double clicking main.exe (file with blue game icon).

  
# Launching the game
----------------------------
After downloading the compressed file, right click the file and select 'Extract All'. 

![ExtractAll](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/ca5f1e2ebe172532099ed63a1ee1f27c4cf938ca/Screenshots/ExtractAll.png)

Click inside the extracted folder. Double click the file named "main.exe" with the blue icon.

![DoubleClick](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/DoubleClick.png)

The game will launch, and 2 windows should appear on your screen. The first is a black console window,
which you can drag to the side for now. Some non-urgent in-game messages will be displayed in this
console window. The other window you will see is the main game interface, where most of the action
will take place.

![Launch](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Launching.png)


# New Game/Load Game
----------------------------
In the main menu, which appears when you first launch the game, you have the option to start a new game
or to continue game (loading from a previously saved slot). If you choose to continue game, you will be
prompted to choose from a save slot. You can mouse over the slot icons to obtain additional information 
about each saved slot.

![LoadGame](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/LoadingSaveSlot.png)


# Map
----------------------------
To travel to different locations, click the 'Map' icon in the menu bar. ![MapIcon](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/MapIcon.png)

![Map](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Map.png)


# Profile
----------------------------
To view information about your character, click the Profile icon from the menu bar. ![ProfileIcon](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/ProfileIcon.png)

On the left side, at the top, you will see your current level, your experience (6582/7200 means you currently
have 6582 experience and will level up once you reach 7200 experience), how many upgrades you currently have,
and your status. 

Below that, you will see your stats and attributes. You can click the green + button to the right of each
stat to increase it. Doing so will require upgrade points, which are obtained by leveling up. The amount of
upgrade points received depends on your level as well as your *perception* attribute. Mouse over each
stat/attribute to view more information about each.

On the right, you will see the special moves and skills that you have learned. You can read more about special moves and skills in the **Combat** section below.

![Profile](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Profile.png)


# Inventory
----------------------------
Click the inventory icon in the menu bar to view your inventory. ![InventoryIcon](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/InventoryIcon.png)

You can mouse over each item to view a description of the items. Clicking on an item selects the item; 
you will then be given options to 'Use', 'Equip'/'Remove', and 'Sell' based on the properties of the item 
you've selected.

![Inventory](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Inventory.png)


### Equipment
----------------------------
If an item in your inventory is an equipment, the 'Equip' option will be enabled when you select the item.
You have a total of 6 equipment slots:

* Head

* Body

* Left Hand

* Right Hand

* Legs

* Feet

To remove an already-equipped item, select the item in your inventory and the 'Equip' option will now be
'Remove' for that item. 

![Equipment](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Equipment.png)


# Saving Game
----------------------------
Click the Save icon from the menu bar to save game. ![SaveIcon](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/SaveIcon.png)

If you have 'quick save' enabled (go to settings to toggle this option on/off), the game will automatically 
be saved to the current slot that you've loaded. Otherwise, you will be prompted to choose a slot to save to. 
If you choose to save on an existing slot, the chosen slot will be overwritten.

You can save the same state in multiple slots. For example, you started the game by loading Slot 2. You
level up after defeating some enemies. You can save this game state to Slot 2 and also Slot 3 (if you wish). 
Note, however, that to choose which slot to save to, you must have 'quick save' DISABLED. If 'quick save' is
enabled, you will save to Slot 2 by default in this scenario.


# Settings
----------------------------
You can toggle the game settings by clicking on the Settings icon from the menu bar. ![SetingsIcon](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/SettingsIcon.png)

Here you will be given options to turn on/off the music and sound effects as well as whether to enable 
'quick save'. Be sure to click 'Confirm' after changing the settings.

![Settings](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Settings.png)


# Dialogue
----------------------------
To interact with NPCs in the game, click on their avatar and choose from the options that appear. 
If you are currently in the middle of a quest which involves the NPC you clicked on, a cutscene
may begin before you are given the options for normal interaction. The decisions you make during 
a quest can impact your chivalry and/or future plot progression.

![Dialogue1](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Dialogue1.png)

Clicking 'Skip Dialogue' will skip all dialogue in the cutscene until a point where you have to make
a decision, if such a point exists in the current cutscene.

![Dialogue2](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/51618304b38eebc453cd918ec58f97471b58c00f/Screenshots/Dialogue2.png)



# Combat
----------------------------

### Stats
----------------------------

There are 7 stats that are directly related to combat:

* Health - you lose the battle if your health reaches 0.

* Stamina - required to execute most attacks. You also receive defence and damage bonuses as your stamina upper limit increases.

* Attack - determines your accuracy

* Strength - determines the damage multiplier that is applied to each move's base damage

* Speed - determines how often you get to move for each of your opponent's moves

* Defence - determines how much damage you take from an opponent's attack

* Dexterity - determines the likelihood of you dodging an attack

You can use upgrade points to increase these stats up to 200 by default. After beating the game once,
if you choose to continue to the next 'playthrough', this limit will increase.


### Using Moves
----------------------------

Use a move by clicking on the button corresponding to the move you wish to execute. If the button
for a move is greyed out, it means it is unavailable (usually due to a negative battle status or
if you don't have enough stamina to execute the move). 

![Battle](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/ff0c25c1d7c6cbcef11861a1363a24055cf8421e/Screenshots/Battle.png)


### Attack Type
----------------------------

There are 3 types of attacks: unarmed, armed, and hidden weapon. Unarmed attacks have no requirements. 
To perform an armed attack, you must have a weapon equipped; otherwise, the special move will be greyed
out. Hidden weapon attacks usually require that you have a certain item in your inventory that will be
consumed each time you use it.


### Action Points
----------------------------
The battle system is largely turn-based but not necessarily a back-and-forth fight. Each turn, you and 
your opponent will receive action points (not visible to player) equal to your speed. The player with
more action points will attack first by using action points equal to the opponent's speed. The remaining
action points are kept for next round's calculation. This means that the higher your speed, the more
often you will get to attack for every attack executed by the opponent. 


### Standard Moves
----------------------------
Standard moves do not require stamina and are available regardless of negative statuses obtained during battle.

* Rest: recover 5% health and 20% stamina (based on upper limit). Equipment and/or effects from skills may increase the actual amount recovered.

* Items: use an item from your inventory. Spends 33% fewer action points.

* Flee: escape from the battle. Chance of success is based on your speed and level difference between you and your opponent. You can only flee from certain battles (spars, non-aggressive enemies, etc.). Otherwise, you will see the message __*'Can't escape from this battle!'*__ in the console window when trying to flee.


### Special Moves
----------------------------
Special moves are usually obtained by training with masters, completing a quest, or studying a martial arts manual.

Most special moves are attacks that require the usage of stamina. You can also level up the proficiency of your attacks
but using them repeatedly, up to level 10. With each proficiency level, the base damage of the attack will
increase by the specified percentage in the move description (mouse over icon), as will the stamina
required to execute the attack. You will also unlock different effects as you level up the attacks.

The amount of experience gained each time you use an attack is based on your perception; whether your attack
hits or misses does not impact the amount of experience gained.


![SpecialMove](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/ff0c25c1d7c6cbcef11861a1363a24055cf8421e/Screenshots/SpecialMove.png)



### Skills
----------------------------
Skills (typically agility techniques or inner energy cultivation techniques) are passive, meaning you do not have to do
anything to 'activate' the effects. Just having the skill means you are already given the bonuses specified in the skill description.
Skills can be leveled up to level 10 by using upgrade points, 1 point for each level. The bonuses received
at each level is listed in the skill description (mouse over skill icon).


### Battle Status
----------------------------
There are statuses you can acquire in battle that impact your (or your opponent's) performance. These are usually
caused by special move effects or weapon effects. Some statuses are permanent, meaning they will last
until the end of the battle unless cleared by effects from a skill, special move, or item. Others last for
a certain number of turns.

* Poison: permanent. At the end of each turn, lose 5% health (based on upper limit). Poison effects can stack, from Poison (-5% health) --> Poison+ (-7.5% health) --> Poison++ (-10% health)


* Internal Injury: permanent. At the end of each turn, lose 5% stamina (based on upper limit). Internal Injury effects can stack, from Internal Injury (-5% health) --> Internal Injury+ (-7.5% health) --> Internal Injury++ (-10% health)


* Blind: limited. Each move's accuracy is reduced by 50% when a person has this status. Number of turns remaining is denoted by the number in parenthesis, i.e. 'Blind (3)' means the person will have this status for 3 turns.


* Seal: limited. All special moves that require stamina are disabled. Number of turns remaining is denoted by the number in parenthesis.


* Stun: when a person is stunned, their next turn is skipped. This is not displayed in the status as the effect is immediate.


* Bleeding: At end of each turn, lose health and stamina equal to opponent's strength.


* Death (X): At the end of each turn, there's an X% chance that character's health will drop to 0 immediately.


Status effects can stack, as shown in the screenshot below:

![BattleStatus](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/ba5bc3a231f61cbbbc810421363b0eac31f4a0e9/Screenshots/BattleStatus.png)


### Critical Hit
----------------------------

Some moves have a chance of inflicting critical hit (move inflicts twice as much damage). Certain weapons/equipment may also increase the probability of critical hits.



### Damage Multiplier
----------------------------

The damage dealt by each attack is determined by its base damage multiplied by the damage multiplier.
Several things can influence the damage multiplier:

* Stats: the higher difference between your strength and the opponent's defence, the larger the damage multiplier


* Equipment: some equipments can boost the damage multiplier


* Stamina upper limit: as previously mentioned, the damage multiplier increases as your stamina upper limit increases.


* Attack type bonus: for each proficiency level in your special moves, moves of a similar type will gain +3% damage multiplier. For example, if you've learned two unarmed special moves and both are at level 5, (5+5)*3 = 30% damage boost.




### Experience
----------------------------

After a battle, you will gain experience based on:

* Whether you won the battle

* The level difference between you and your opponent

### Death
----------------------------

When your health reaches 0, you will lose the battle and die unless you are sparring or if you are in training. If this happens, you will be taken back to the main menu. Any unsaved progress will be lost.


# Minigames 
----------------------------
There are a variety of minigames available. The platformer-type minigames often require you to jump to navigate through the level. 
The maximum height of your jumps is determined by the sum of your speed and dexterity stats. 
When you die in a minigame, you will be able to retry the game or quit to main menu (which will be the equivalent of dying in battle/unsaved progress will be lost).

![Minigame](https://bitbucket.org/xiaohanhua/wuxia-world-readme/raw/fa99c367480aefb8b623815db3ec46ee0969bd8d/Screenshots/Minigame.png)


# Attribution
 
----------------------------
### **Images**

https://unsplash.com/photos/de0P588zgls Photo by Tom Coomer on Unsplash

https://unsplash.com/photos/VCRci_usn0w Photo by Mirko Blicke on Unsplash

https://unsplash.com/photos/_OQ8Jc7kBmA Photo by JR Korpa on Unsplash

https://unsplash.com/photos/1L71sPT5XKc Photo by Rosie Fraser on Unsplash

https://unsplash.com/photos/v-4hCZPj1Ks Photo by Yifan Liu on Unsplash

https://unsplash.com/photos/l6dDiMFabrg Photo by Justin Lim on Unsplash

https://unsplash.com/photos/7Jkb1_Lg2Eg Photo by Jimmy Chang on Unsplash

https://unsplash.com/photos/caS4xEOtL_k Photo by Ezra Jeffrey-Comeau on Unsplash

https://unsplash.com/photos/fvbVdB5n2I8 Photo by Michael Brunk on Unsplash

https://unsplash.com/photos/sp-p7uuT0tw Photo by Sebastian Unrau on Unsplash

https://unsplash.com/photos/2Y4dE8sdhlc Photo by Irina Iriser on Unsplash

https://unsplash.com/photos/JxR2QUrEuaQ Photo by Mike Petrucci on Unsplash

https://unsplash.com/photos/vsxOcxtWig4 Photo by Adrian Pereira on Unsplash

https://unsplash.com/photos/-HDbl_akROQ Photo by Annie Spratt on Unsplash

https://unsplash.com/photos/fzHaSRdAi68 Photo by Masaaki Komori on Unsplash

https://unsplash.com/photos/_41JjnrSeHU Photo by Yustinus Subiakto on Unsplash

https://unsplash.com/photos/K8g-HOaJaxw Photo by Stacey Gabrielle Koenitz Rozells on Unsplash

http://img.1ppt.com/uploads/allimg/1712/1_171203090756_1.jpg Painting by ?

https://unsplash.com/photos/s20nA2KB1N4 Photo by Andrey Andreyev on Unsplash

https://unsplash.com/photos/jh2KTqHLMjE Photo by Jeremy Thomas on Unsplash

https://unsplash.com/photos/aCHBgtcE7D8 Photo by silvana amicone on Unsplash

https://unsplash.com/photos/Cm5zI68Wdew Photo by Eugene Triguba on Unsplash

https://unsplash.com/photos/SwbsW4uf1Qs Photo by Nick Fewings on Unsplash

https://unsplash.com/photos/Eydo2lQNfgU Photo by 301+ Kim on Unsplash

https://unsplash.com/photos/xvzslxskOw4 Photo by Krisztián Korhetz on Unsplash

https://unsplash.com/photos/ULYY9HMKI6Q Photo by Timur M on Unsplash

https://imgsrc.baidu.com/baike/pic/item/7acb0a46f21fbe090e7a270a64600c338744ad8c.jpg

http://a4.att.hudong.com/62/90/01300000025788122195906673825_s.jpg 

https://www.flickr.com/photos/13798849@N00/299376335 "Kimono Model" by Stefan_- is licensed under CC BY-NC-ND 2.0. 

To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-nd/2.0/

https://upload.wikimedia.org/wikipedia/commons/c/c3/Kusarigama.jpg 

https://unsplash.com/photos/Y_e_3q4bErg Photo by zhu wei on Unsplash


《射雕英雄传》 2017

《天下第一》 2005

《倚天屠龙记》 2019

《河洛群侠传》

《锦衣卫之初露锋芒》


 
----------------------------
 
 
 
### **Sprites/Icons**

https://opengameart.org/content/flare-hud (Mumu)

https://opengameart.org/content/rpg-gui-contstruction-kit-vmoon (Under the moon)

https://opengameart.org/content/flare-item-variation-60x60-only (Mumu)

https://opengameart.org/content/hero-spritesheets-ars-notoria (Balmer)

https://opengameart.org/content/asteroids (phaelax)

https://opengameart.org/content/texture-to-tile-project (The Chayed)

https://opengameart.org/content/platformer-or-fighter-sprites (Fracture)

https://opengameart.org/content/four-characters-my-lpc-entries (Redshrike, Stephen Challener)

https://opengameart.org/content/chest-and-mimic (Redshrike, Stephen Challener)

https://opengameart.org/content/country-side-platform-tiles (ansimuz)

https://opengameart.org/content/lpc-heroine-2 (Yamilian)

https://opengameart.org/content/lpc-heroine (Yamilian)

https://opengameart.org/content/armor-icons-by-equipment-slot-with-transparency (Clint Bellanger, Blarumyrran, crowline, Justin Nichol)

https://opengameart.org/content/wandering-vendor-npc (Tiziana)

https://opengameart.org/content/isometric-hero-and-creatures (Clint Bellanger)

https://opengameart.org/content/pixel-art-contest-entry-armor-and-hair-for-anime-style-base (LokiF)

https://opengameart.org/content/cabbit-collection (AntumDeluge)

https://opengameart.org/content/shuriken-pixel-art (Loel)

https://opengameart.org/content/jump-and-run-tileset-24x24 (tamashihoshi)

https://opengameart.org/content/light-themed-weapons (ScratchIO)

https://opengameart.org/content/spike-ball © 2005-2013 Julien Jorge <julien.jorge@stuff-o-matic.com>

https://opengameart.org/content/skill-item-and-spell-icons (pauliuw)

https://opengameart.org/content/walking-ant-with-parts-and-rigged-spriter-file (DudeMan)

https://opengameart.org/content/spider-flare-sprite-sheets (Wciow and John.d.h)

https://opengameart.org/content/weapon-icons-32x32px-painterly (Scrittl)

https://opengameart.org/content/armor-icons-32x32-px-painterly (Scrittl)

https://opengameart.org/content/character-equipment-slots (lukems-br)

https://opengameart.org/content/gold-treasure-icons (Clint Bellanger)

http://opengameart.org/users/varkalandar (Hansjörg Malthaner)

https://opengameart.org/content/various-stones-and-oregem-veins-16x16 (Senmou)

https://opengameart.org/content/poison-skull (Jorge Avila)

https://opengameart.org/content/arcane-magic-effect (Cethiel)

https://opengameart.org/content/castle-door (Castle door by Tuomo Untinen)

https://opengameart.org/content/whirlwind (Spring)

https://opengameart.org/content/lightning-shock-spell (Clint Bellanger)

https://opengameart.org/content/recursive-cave-entrance (Makrohn)

https://opengameart.org/content/conveyor-belts-spritesheet-anims (Color Optimist)

https://opengameart.org/content/spikey-stuff (dravenx)

https://opengameart.org/content/skeleton-0 (Andrew Garcell)

https://opengameart.org/content/lpc-animated-torch (William.Thompsonj, Hugh Spectrum, and Sharm)

https://opengameart.org/content/a-creepy-dead-tree (pixeltroid)

https://opengameart.org/content/cementery-gate (zerberros)

https://opengameart.org/content/parchment (Mattias Lejbrink)

https://opengameart.org/content/knarled-trees (Onsemeliot)

https://opengameart.org/content/key-icons (BizmasterStudios)

https://opengameart.org/content/shovel-1 (AntumDeluge)

https://opengameart.org/content/the-spookiest-skeleton (RedVoxel)

https://opengameart.org/content/samurai-japan (WarmGuy)

《三国杀Online》https://web.sanguosha.com/ 

《剑网3》https://jx3.xoyo.com/

《九阴九阳》https://www.9game.cn/jyjy/

《笑傲江湖OL》http://xa.wanmei.com/

《铁血武林》,《武林英雄传》https://txwl.hcplay.com.cn/

https://neverwintervault.org/project/nwn2/images/icons/new-nwn2-icons

https://neverwintervault.org/project/nwn1/images/icons/new-bag-icons

http://dustychest.blogspot.com/ (DustyChest@gmail.com)

http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/ (GPL3 and CC-BY-SA3)

https://opengameart.org/content/arrow-0 (Michael J Pierce)

http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220758840424&categoryNo=191&parentCategoryNo=0# (zzangogo7)

https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220803168997&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)

https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220755375870&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)

https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220629596177&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)

https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220617905735&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)

https://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220747533426&categoryNo=&parentCategoryNo=186&from=thumbnailList (zzangogo7)

https://en.wikipedia.org/wiki/File:Male_Missulena_occatoria_spider_-_cropped.JPG (https://creativecommons.org/licenses/by-sa/3.0/deed.en)

https://en.wikipedia.org/wiki/File:01_Schwarzb%C3%A4r.jpg (https://creativecommons.org/licenses/by-sa/3.0/deed.en)

https://en.wikipedia.org/wiki/File:Echis_carinatus_sal.jpg (https://creativecommons.org/licenses/by/2.5/deed.en)

https://commons.wikimedia.org/wiki/File:Steinl%C3%A4ufer_(Lithobius_forficatus)_1.jpg (https://creativecommons.org/licenses/by-sa/2.5/deed.en)

https://commons.wikimedia.org/wiki/File:Saturnid_moth_(Lonomia_electra).jpg (https://creativecommons.org/licenses/by-sa/4.0/deed.en)

https://commons.wikimedia.org/wiki/File:Hyles_gallii,_Lodz(Poland)04(js).jpg (https://creativecommons.org/licenses/by-sa/3.0/deed.en)

https://en.wikipedia.org/wiki/Automeris_io#/media/File:Automeris_ioFMPCCA20040704-2974B1.jpg (https://creativecommons.org/licenses/by-sa/2.5/deed.en)

https://en.wikipedia.org/wiki/Cinnabar_moth#/media/File:Cinnabar_moth_(Tyria_jacobaeae).jpg (https://creativecommons.org/licenses/by-sa/4.0/deed.en)

https://en.wikipedia.org/wiki/Saturniidae#/media/File:Saturnia_pavonia_01.jpg (https://creativecommons.org/licenses/by-sa/3.0/deed.en)

https://en.wikipedia.org/wiki/Centipede#/media/File:Steinl%C3%A4ufer_(Lithobius_forficatus)_1.jpg (https://creativecommons.org/licenses/by/2.5/deed.en)

https://ccsearch.creativecommons.org/photos/6eabb363-1b42-41b1-9f9b-c6c6dfd300c5 "Scolopendra Heros" by CykoFlickr 
is licensed under CC BY-NC 2.0 (https://creativecommons.org/licenses/by-nc/2.0/?ref=ccsearch)

http://www.softicons.com/toolbar-icons/soft-scraps-icons-by-deleket/save-icon (stockicons@deleket.com)

https://bkimg.cdn.bcebos.com/pic/f603918fa0ec08fa16dffd7657ee3d6d54fbda00?x-bce-process=image/watermark,g_7,image_d2F0ZXIvYmFpa2UxMTY=,xp_5,yp_5

http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220831287447&categoryNo=&parentCategoryNo=186&from=thumbnailList

http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220620529163&categoryNo=&parentCategoryNo=186&from=thumbnailList

http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220663655095&categoryNo=&parentCategoryNo=186&from=thumbnailList

http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220620057635&categoryNo=0&parentCategoryNo=186

http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220626106747&categoryNo=0&parentCategoryNo=186

https://en.wikipedia.org/wiki/Tai_chi#/media/File:Taijiquan_Symbol.png

https://open3dmodel.com/wp-content/uploads/2019/04/20190419_5cb9dd47c14ee.jpg

----------------------------

 
 
 
### **Sound Effects**

https://www.fesliyanstudios.com

http://soundbible.com/

https://www.zapsplat.com/

https://www.soundjay.com/

https://freesound.org/people/AmeAngelofSin/sounds/264982/

https://opengameart.org/content/large-monster (Michael Klier)

https://opengameart.org/content/sea-and-river-wave-sounds (RandomMind)

http://soundbible.com/2053-Thunder-Sound-FX.html (Grant Evans)

http://opengameart.org/users/varkalandar (Hansjörg Malthaner)

https://www.freesound.org/people/tcrocker68/sounds/235592/

https://opengameart.org/content/male-gruntyelling-sounds (HaelDB)

https://opengameart.org/content/rain-loopable (Ylmir)

https://opengameart.org/content/thunder-very-close-rain-01 (Inspector J)

https://www.freesoundeffects.com/free-sounds/thunder-sounds-10040/

https://opengameart.org/content/ghost (Ogrebane)

https://opengameart.org/content/evil-creature (A New Room)

https://opengameart.org/content/horror-scream1 (Vinrax)

https://freesound.org/people/hykenfreak/sounds/331621/

https://opengameart.org/content/35-wooden-crackshitsdestructions (Independent.nu)

https://freesound.org/people/MorneDelport/sounds/326407/ (MorneDelport)

https://opengameart.org/content/whip-sound (Whipping sound by Tuomo Untinen)

http://freesound.org/people/CGEffex/sounds/98341/ (CGEffex)

笑傲江湖 2001电视剧
 
 
 
----------------------------
 
 
 
### **Music**

金庸群侠传 (蔡志展)

水浒传之梁山好汉游戏

仙剑奇侠转 (蔡志展)

世间始终你好 （罗文，甄妮）

罗汉阵 (Rock Records 滾石國際音樂股份有限公司)

初识太极 (Rock Records 滾石國際音樂股份有限公司)

男儿当自强 （鲍比达，黄沾）

《金田一少年事件薄》 (见岳章)

欢沁 （林海）

琵琶语 （林海）

小鱼儿的思绪 (麦振鸿)

飘絮与天涯 (麦振鸿)

缘尽大海 (智冠超)

误入迷失森林 (韦启良)

张根失踪 (韦启良)

心殇 (麦振鸿)

波折

三个人的时光 (曾志豪，吴欣睿)


----------------------------
### **Other**


Maze Generator code: https://github.com/timothygordon32/mazes/blob/master/maze.py
